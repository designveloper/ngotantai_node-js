let Express = require("express");
// let exphbs = require("express-handlebars");
let bodyParser = require("body-parser");
let multer = require("multer");
let fs = require("fs"),
  request = require("request");
const path = require("path");

let app = Express();


app.use(bodyParser.json());
app.use('/Images', Express.static(path.resolve(__dirname, "Images")));

let Storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./Images");
  },
  filename: function (req, file, cb) {
    let imageUrl = file.fieldname + "_" + Date.now() + "_" + file.originalname;
    cb(null, imageUrl);

  }
});
app.get("", function (req, res) {
  res.sendFile(__dirname + "/upload.html");
});

app.post("/upload", function (req, res) {
  let upload = multer({
    storage: Storage
  }).array("imgUploader", 3);

  upload(req, res, function (err) {
    if (!err) {
      res.end("localhost:5001/Images/" + res.req.files[0].filename);
    } else
      return res.end("St went wrong!" + err);
  });
});

app.listen(5001, function () {
  console.log("server is running");
});